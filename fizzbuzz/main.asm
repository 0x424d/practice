; Assembled with NASM on Linux

SECTION .data                                   ; Section for initialised data
	fizz DB "Fizz", 0xa                     ; 0xa is a newline
	fizzLen EQU $ - fizz                    ; This gets the length of the string

	buzz DB "Buzz", 0xa
	buzzLen EQU $ - buzz

	fizzbuzz DB "FizzBuzz", 0xa
	fizzbuzzLen EQU $ - fizzbuzz

	myTest DB "Placeholder", 0xa
	testLen EQU $ - myTest

SECTION .text				        ; Section for actual code
	GLOBAL _start			        ; equivalent to C's main() - requires global scope to be seen by the linker

_start:
	MOV cx, 1				; Move 1 into cx (loop counter)
	PUSH cx					; Remember cx (popped later)
	JMP CheckFizzBuzz           	        ; Unconditional jump to CheckFizzBuzz

	CheckFizzBuzz:
		MOV dx, 0			; Move 0 into dx
		MOV ax, cx			; Move dividend (loop counter) into ax
		MOV bx, 3			; Move first divisor (3) into bx
		DIV bx				; Divide dx:ax by bx; result stored in ax (quotient) and dx (remainder)
		CMP dx, 0			; Compare dx to 0
		JNE CheckFizz	                ; If not equal, jump to CheckFizz
		MOV dx, 0
		MOV ax, cx			; If equal, move dividend back into ax
		MOV bx, 5			; Move second divisor (5) into bx
		DIV bx				; Once again divide dx:ax by bx; result stored in ax (quotient) and dx (remainder)
		CMP dx, 0			; Compare dx to 0
		JNE CheckFizz	                ; If not equal, jump to CheckFizz
		CALL PrintFizzBuzz	        ; If equal, jump to PrintFizzBuzz
		POP cx				; Put stored loop counter value into cx
		PUSH cx				; Push it back onto the stack
		JMP Loop			; Unconditional jump to Loop

	CheckFizz:
		MOV dx, 0			; Division with a 16-bit divisor takes values from dx:ax
		MOV ax, cx			; Move dividend (loop counter) into ax
		MOV bx, 3			; Move divisor (3) into bx
		DIV bx				; divide dx:ax by bx; result stored in ax (quotient) and dx (remainder)
		CMP dx, 0			; Compare dx to 0
		JNE CheckBuzz	    	        ; If not equal, jump to CheckBuzz
		CALL PrintFizz		        ; If equal, call PrintFizz
		POP cx				; Set cx back to loop counter (instead of half of fizz)
		PUSH cx				; And push the correct number back on the stack
		JMP Loop			; Unconditional jump to Loop (only executed if dx == 0)

	CheckBuzz:
		MOV dx, 0
		MOV ax, cx			; Move dividend (loop counter) into ax
		MOV bx, 5			; Move divisor (5) into bx
		DIV bx				; divide dx:ax by bx; results stored in ax (quotient) and dx (remainder)
		CMP dx, 0			; Compare dx to 0
		JNE PrintNum		        ; If not equal, jump to PrintNum
		CALL PrintBuzz		        ; If equal, jump to PrintBuzz
		POP cx
		PUSH cx
		JMP Loop			; Unconditional jump to Loop

	PrintFizzBuzz:
		MOV edx, fizzbuzzLen
		MOV ecx, fizzbuzz
		MOV ebx, 1
		MOV eax, 4
		INT 0x80
		RET

	PrintFizz:
		MOV edx, fizzLen    	        ; Move fizzLen into edx
		MOV ecx, fizz		        ; Move fizz into ecx
		MOV ebx, 1			; Move file descriptor (1, stdout) into ebx
		MOV eax, 4			; Move syscall id (4, write()) into eax
		INT 0x80			; System call 4 (write())
		RET			        ; Return to caller

	PrintBuzz:
		MOV edx, buzzLen
		MOV ecx, buzz
		MOV ebx, 1
		MOV eax, 4
		INT 0x80			; System call 4 (write())
		RET			        ; Return to caller

	PrintNum:
		MOV edx, testLen    	        ; Move testLen into edx
		MOV ecx, myTest		        ; Move test into ecx
		MOV ebx, 1			; Move file descriptor (1, stdout) into ebx
		MOV eax, 4			; Move syscall id (4, write()) into eax
		INT 0x80			; System call 4 (write())
		JMP Loop			; Unconditional jump to PrintNewline

	Loop:
		POP cx				; Pop loop counter from top of stack, place it in cx
		INC cx				; Increment cx (loop counter)
		PUSH cx				; Push cx
		CMP cx, 101			; Compare cx to 101
		JNE CheckFizzBuzz	        ; If not equal, jump back to CheckFizz

		MOV eax, 1			; If equal, move 1 into eax
		MOV ebx, 0			; Move 0 into ebx
		INT 0x80			; System call 1 (exit())
